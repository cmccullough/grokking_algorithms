package main

import (
	"fmt"
	"gitlab.com/cmccullough/grokking_algorithms/util"
	"log"
	"os"
	"time"
)

var logger *log.Logger

func init() {
	logger = log.New(os.Stdout, "", 0666)
}

func main() {
	/**
	Head and Tail recursion in Golang.
	 */
	intsToSum := []int{1, 2, 3, 4, 5}

	finalSum := sum(intsToSum)
	finalLoopSum := loopSum(intsToSum)

	fmt.Println("Final sums:", finalSum, finalLoopSum)
}

func sum(list []int) int {
	defer util.TimeTrack(logger, time.Now(), "recursive sum")

	if len(list) < 1 {
		return 0
	}

	return recurseSum(0, list)
}

func recurseSum(head int, tail []int) int {
	if len(tail) <= 0 {
		return head
	}

	return head + recurseSum(tail[0], tail[1:])
}

func loopSum(list []int) int {
	defer util.TimeTrack(logger, time.Now(), "loop sum")

	finalSum := 0
	for _, v := range list {
		finalSum += v
	}
	return finalSum
}
