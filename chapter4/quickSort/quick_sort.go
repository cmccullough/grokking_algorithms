package main

import (
	"fmt"
	"gitlab.com/cmccullough/grokking_algorithms/util"
	"log"
	"math/rand"
	"os"
	"time"
)

var logger *log.Logger

func init() {
	logger = log.New(os.Stdout, "", 0666)
}

func main() {
	/**
		QuickSort in Golang.
	 */
	fmt.Println("QuickSort in Golang.")

	defer util.TimeTrack(logger, time.Now(), "recursive quicksort")

	largeRandomSlice := make([]int, 0, 1000)
	for i := 0; i < 1000; i++ {
		largeRandomSlice = append(largeRandomSlice, rand.Intn(1000))
	}

	fmt.Println(largeRandomSlice)
	sortedRandom := quickSort(largeRandomSlice)
	fmt.Println("Final sorted list:", sortedRandom)
}

func quickSort(list []int) []int {
	if len(list) < 2 {
		return list
	}

	pivot := list[0]

	lessThan := make([]int, 0, len(list))
	greaterThan := make([]int, 0, len(list))

	for _, v := range list[1:] {
		if v <= pivot {
			lessThan = append(lessThan, v)
		} else {
			greaterThan = append(greaterThan, v)
		}
	}
	return append(append(quickSort(lessThan), pivot), quickSort(greaterThan)...)
}
