package util

import (
	"log"
	"time"
)

func TimeTrack(logger *log.Logger, start time.Time, name string) {
 	elapsed := time.Since(start)
	logger.Printf("%s took %s", name, elapsed)
}