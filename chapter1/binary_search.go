package main

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/cmccullough/grokking_algorithms/util"
	"log"
	"os"
	"time"
)

var logger *log.Logger

func init() {
	logger = log.New(os.Stdout, "", 0666)
}

func main() {
	myList := []int{1, 3, 5, 7, 9}

	index, err := binarySearch(myList, 3)
	if err != nil {
		log.Println(err)
	}

	fmt.Println("Item", 3, "found at index", index)

	index, err = binarySearch(myList, 4)
	if err != nil {
		log.Println(err)
	}
}

func binarySearch(list []int, item int) (int, error) {
	defer util.TimeTrack(logger, time.Now(), "binarySearch")

	low := 0
	high := len(list) - 1

	for low <= high {
		mid := (low + high) / 2
		guess := list[mid]
		if guess == item {
			return mid, nil
		} else if guess > item {
			high = mid - 1
		} else if guess < item {
			low = mid + 1
		}
	}
	return -1, errors.New("item was not found in list!")
}