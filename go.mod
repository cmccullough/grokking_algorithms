module gitlab.com/cmccullough/grokking_algorithms

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/labstack/gommon v0.2.8 // indirect
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/pkg/errors v0.8.0
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v0.0.0-20170224212429-dcecefd839c4 // indirect
	gitlab.com/cmccullough/grokking_algorithms/util v0.0.0-20181224163055-853bb206ceb3
	golang.org/x/sys v0.0.0-20181221143128-b4a75ba826a6 // indirect
)
