package main

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/cmccullough/grokking_algorithms/util"
	"log"
	"os"
	"time"
)

var logger *log.Logger

func init() {
	logger = log.New(os.Stdout, "", 0666)
}

func main() {
	fmt.Println("Sorting a List with Selection Sort in Go.")
	listToSort := []int{5, 3, 6, -2, 10}

	sortedList, err := selectionSort(listToSort)
	if err != nil {
		log.Fatal("An error occurred while selection sorting:", err)
	}

	fmt.Println("And now, the sorted list:", sortedList)
}

func findSmallest(list []int) (int, error) {
	if len(list) <= 0 {
		return -1, errors.New("Must provide a non empty list")
	}

	smallest := list[0]
	smallestIndex := 0

	for i, v := range list {
		if v < smallest {
			smallestIndex = i
			smallest = v
		}
	}

	return smallestIndex, nil
}

func selectionSort(list []int) ([]int, error) {
	defer util.TimeTrack(logger, time.Now(), "selectionSort")

	newList := make([]int, 0, len(list))

	for _ = range list {

		smallestIndex, err := findSmallest(list)
		if err != nil {
			return newList, err
		}

		// Append value to list
		newList = append(newList, list[smallestIndex])

		// Modify original list to remove the element we just found
		list = append(list[:smallestIndex], list[smallestIndex+1:]...)
	}

	return newList, nil
}
